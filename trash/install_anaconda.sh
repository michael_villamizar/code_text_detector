#!/bin/bash

# Temporal folder.
cd /tmp

# Download Anaconda.
curl -O https://repo.continuum.io/archive/Anaconda3-5.0.1-Linux-x86_64.sh

# Check.
sha256sum Anaconda3-5.0.1-Linux-x86_64.sh

# Install anaconda.
bash Anaconda3-5.0.1-Linux-x86_64.sh
