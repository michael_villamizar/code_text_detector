# Activate environment.
conda create -n text_detector python=2.7

# Activate environment
source activate text_detector

# Solve problem with pip.
conda install -c conda-forge pip

# Install TensorFlow CPU.
pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.2.1-cp27-none-linux_x86_64.whl

# Install TensorFlow GPU: It requires cuda 8.0 and cudnn 5.
#pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-1.2.1-cp27-none-linux_x86_64.whl
