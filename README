# **Text Detection Network**

The goal of this notebook is to build, train and test a **deep network** for
**text detection**. The input of the network is a color image containing text
while the output are **bounding boxes** indicating the **location** of text
inside images.

![text_detection](./imgs/detection.png)

The training and test of the network is done using a **synthetic text dataset**
containing random words over diverse and challenging backgrounds. 

---

### **Dependencies**:
This notebook requires the following libraries:
+ python
+ jupyter
+ numpy
+ tensorflow
+ matplotlib
+ PIL

---

### **Usage**:

Use the following command to run the notebook:

```python
jupyter notebook text_detector.ipynb
```
---

### **License**:

```
Copyright (C) <2019> <Michael Villamizar>

This work is licensed under the Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy
of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or
send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Michael Villamizar. 2019

---

### **Contact**:

**Michael Villamizar** <br>
mvillamizar@idiap.ch <br>
Idiap Research Institute - Switzerland 2019 <br>

